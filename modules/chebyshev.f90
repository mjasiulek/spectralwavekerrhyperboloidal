module chebyshev

   ! Module providing transformation matrices to compute Chebyshev coefficients
   ! or function values / function's derivatives in physical space

   ! This code is easier to read with the editor VIM and folding: 
   ! set foldmethod=marker, set foldmarker={{{,}}} in .vimrc
   implicit none

   private
   public :: init_chebyshev 
   public :: coeff_to_dcoeff 

   ! Define variables {{{  
   real (kind=8), parameter :: pi = 3.14159265358979323846264338327950288d0

   real (kind=8), allocatable, save, public  :: Tnk(:,:), iTnk(:,:), diTnk(:,:), ddiTnk(:,:) 

   character*100 ::  formstr

   !}}}
contains

subroutine coeff_to_dcoeff( ak, bk, nrr ) !{{{
   ! Compute coefficients of function's derivative from function's coefficients
   implicit none

   ! b_k = b_{k+2} + 2*a_{k+1}, k=1,nrr
   ! df(x)/dx = \Sum''^nrr_k=1 b_k T_k(x) ; \Sum'' including factors of 1/2 for k=1, k=nrr

   real (kind=8), intent(in)  :: ak(nrr)
   real (kind=8), intent(out) :: bk(nrr)

   integer, intent(in)        :: nrr

   integer :: n, k

   bk(nrr)   = 0d0
   bk(nrr-1) = 2d0*(nrr-1)*ak(nrr)

   do k=2,nrr-2
      n = nrr - k
      bk(n) = bk(n+2) + 2d0*n*ak(n+1)  
   end do

   bk(1) = bk(3) + 2d0*ak(2) 
   bk(nrr-1) = bk(nrr-1)/2d0 ! because factors of 1/2 are include in matrix Tnk(nrr,nrr) 

end subroutine coeff_to_dcoeff !}}}

subroutine init_chebyshev(nrr) !{{{
   ! Compute transformation matrices to transform between physical and
   ! Chebyshev-spectral space for an interval on nrr points

   implicit none

   integer, intent(in) :: nrr

   real (kind=8)      :: t0
   real (kind=8)      :: Tnk0(nrr,nrr), diTnk0(nrr,nrr), tmp 

   integer :: k, n

   if ( .not. allocated( Tnk ) ) then
      allocate(   Tnk(nrr,nrr) )
      allocate(  iTnk(nrr,nrr) )
      allocate( diTnk(nrr,nrr) )
      allocate(ddiTnk(nrr,nrr) )

      ! Compute Chebyshev poly. at collocation points
      forall (n=1:nrr, k=1:nrr) Tnk0(n,k) = cos((n-1)*pi*(k-1)/(nrr-1))

      ! Include numerical factors
      Tnk = Tnk0*sqrt( 2d0/(nrr-1)); 
      Tnk(:,1  ) = Tnk(:,1  )*0.5d0
      Tnk(:,nrr) = Tnk(:,nrr)*0.5d0
   
      ! Assemble inverse transformation
      iTnk = transpose( Tnk0 )*sqrt( 2d0/(nrr-1))
      iTnk(:,1  ) = iTnk(:,1 )*0.5d0
      iTnk(:,nrr) = iTnk(:,nrr)*0.5d0
   
      ! Compute 1.derivates of Chebyshev poly. dP(x)/dx where x = cos( t ) !{{{
      do k=1,nrr
   
         if ( k==1 ) then
   
            do n=1,nrr
               diTnk0(k,n) = (n-1)**2 
            end do
   
         else if ( k==nrr ) then
   
            do n=1,nrr
               diTnk0(k,n) = (n-1)**2 *(-1)**n
            end do
   
         else 
   
            t0 = Pi*(k-1)/(nrr-1)
            do n=1,nrr
               diTnk0(k,n)  = (n-1)*sin( (n-1)*t0 ) / sin( t0 ) 
            end do
   
            ! Alternative using recurrence relation
          ! diTnk0(k,1) = 0d0; diTnk0(k,2) = 1d0; diTnk0(k,3) = 4d0*cos( t0 )
          ! do n=4,nrr
          !    diTnk0(k,n) = (n-1)*( 2d0*Tnk0(n-1,k) + diTnk0(k,n-2)/(n-3)  )
          ! end do
   
         end if
      end do !}}}
   
      diTnk = diTnk0*Sqrt( 2d0/(nrr-1))
      diTnk(:,1)   = diTnk(:,1  )*0.5d0
      diTnk(:,nrr) = diTnk(:,nrr)*0.5d0
   
      ! Compute 2.derivates of Chebyshev poly. !{{{
      do k=1,nrr
   
         if ( k==1 ) then
   
            do n=1,nrr
               ddiTnk(k,n) = (n-1)**2/3d0 *((n-1)**2-1d0) 
            end do
   
         else if ( k==nrr ) then
   
            do n=1,nrr
              ddiTnk(k,n) = (n-1)**2/3d0 *((n-1)**2-1d0) *(-1)**(n-1)
            end do
   
         else 
   
            t0 = Pi*(k-1)/(nrr-1)
            do n=1,nrr
               ddiTnk(k,n)  = ( - sin(t0)*((n-1)**2 *cos((n-1)*t0)) + cos(t0)*(n-1)*sin((n-1)*t0) )/sin(t0)**3 
            end do

            ! Alternative using recurrence relation
          ! ddiTnk(k,1) = 0d0; ddiTnk(k,2) = 0d0; ddiTnk(k,3) = 4d0
          ! do n=4,nrr
          !    ddiTnk(k,n) = (n-1)*( 2d0*diTnk0(k,n-1) + ddiTnk(k,n-2)/(n-3) )
          ! end do
   
         end if
   
      end do !}}}

      ddiTnk        = ddiTnk* sqrt( 2d0/(nrr-1))
      ddiTnk(:,1)   = ddiTnk(:,1)*0.5d0
      ddiTnk(:,nrr) = ddiTnk(:,nrr)*0.5d0
   end if

end subroutine init_chebyshev !}}}

end module chebyshev

