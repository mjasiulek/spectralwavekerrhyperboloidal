module spectral_wkh

   ! 3d spectral code to evolve wave equation on Kerr background using
   ! hyperboloidal slices, see http://arxiv.org/abs/1109.2513

   ! This code is easier to read with the editor VIM and folding: 
   ! set foldmethod=marker, set foldmarker={{{,}}} in .vimrc

   use sht_tools  ! routines involving spherical harmonics
   use chebyshev  ! routines involving Chebyshev polynomials

   implicit none

   public :: main

   ! Set Parameters !{{{
   integer, parameter :: nrr = 70   ! radial grid points 
   integer, parameter :: nth = 6    ! angular grid points, nph= 2*nth

   real (kind=8), parameter :: mass = 1d0    ! Kerr mass param.
   real (kind=8), parameter :: spin = 0.1d0  ! Kerr spin param.

   real (kind=8), parameter :: rmin = 1.8d0  ! radial interval, r in [rmin,rmax]
   real (kind=8), parameter :: rmax = 20d0   ! rmax at scri+

   real (kind=8), parameter :: dtime = 0.1d0    ! = delta( time ) < CFL*delta( space ) 
   real (kind=8), parameter :: t_final = 600d0  ! termination time 
   integer, parameter       :: itmax = 15000    ! max iterations
   integer, parameter       :: out_every = 5    ! output data every X iterations

   character (len=20) :: outfilename = "/tmp/testd0.asc"

   ! Dependent, fixed parameters
   real (kind=8), parameter :: rtrans = rmin ! transition point different from rmin not yet implemented (requires two spectral domains) 
   integer, parameter :: nph = 2*nth
   integer, parameter :: lmax = nth - 1      ! since Gauss-Legendre grid 
   integer, parameter :: nmax = (lmax+1)**2  ! total number of polynomials up to lmax
   real (kind=8), parameter:: aa_r = (rmax-rmin)/2d0 ! paras for Chebyshev decomposition
   real (kind=8), parameter:: bb_r = rmin + aa_r

   integer, parameter :: nvars = 5           ! number of evolution variables

   real (kind=8), parameter :: pi = 3.14159265358979323846264338327950288d0
   real (kind=8), parameter :: del3(3,3) = reshape((/ 1d0,0d0,0d0,   0d0,1d0,0d0,   0d0,0d0,1d0 /), (/3,3/))
   !}}}

   ! Define variables {{{  

   ! Index arrays
   integer            :: nf(0:lmax,-lmax:lmax), lf(1:nmax), mf(1:nmax)  ! to translate beween (l,m) and (n) where n in [1,nmax]

   ! 1D
   real (kind=8)      :: xk(1:nrr), rr(1:nrr), th_GL(1:nth), omega(1:nrr)
   ! 2D
   real (kind=8)      :: nx(nth,nph), ny(nth,nph), nz(nth,nph), th(nth,nph), ph(nth,nph)
   ! 3D
   real (kind=8)      :: k1(nvars,nrr,nth,nph), k2(nvars,nrr,nth,nph), k3(nvars,nrr,nth,nph), k4(nvars,nrr,nth,nph) ! for Runge-Kutta time integration 

   real (kind=8)      :: vars0(nvars,nrr,nth,nph), vars(nvars,nrr,nth,nph) ! evolution variables (var0: initial data)
   integer, parameter :: psi=1, rho=2, psix=3, psiy=4, psiz=5

   real (kind=8)      :: gi(0:3,0:3,nrr,nth,nph), Gam(0:3,nrr,nth,nph), Ric(nrr,nth,nph)  ! 4-metric, gammas, Ricci scalar

   real (kind=8)      :: dpsi(3,nrr,nth,nph) ! spatial derivatives of scalar field psi
   real (kind=8)      :: debug(nrr,nth,nph)  ! for debugging

   ! Coefficients spectral space
   real (kind=8)      :: ank_var(1:nmax,1:nrr) 
   real (kind=8)      :: ank_tmp(1:nmax,1:nrr) 
   real (kind=8)      :: ank_tmp2(1:nmax,1:nrr) 

!  complex (kind=8)   :: alm_fct(0:lmax,-lmax:lmax)
!  complex (kind=8)   :: clm_tmp(0:lmax,-lmax:lmax)

   complex (kind=8)   :: Nullj(1:3,0:lmax,-lmax:lmax) 

   character*100      ::  formstr

   !}}}
contains

subroutine main

   implicit none

   ! Define local variables {{{
   real (kind=8)      :: h, h2, h6
   real (kind=8)      :: ni(3)  
   real (kind=8)      :: tmp, tmp2, tmp3, tmp4, tmp5, dtmp(3), r0
   real (kind=8)      :: num_acc, check_sum 
   real (kind=8)      :: lnorm(0:lmax) 

   real (kind=8)      :: amp, sig, x0, y0, z0

   integer            :: i,j,k, l,m,n
   integer            :: lf0, ind_out(10)
   integer            :: it
   character (len=20) :: ci !}}}

   if (lmax > 24) print *, "Warning! lmax > 24!"

   ! Set coordinates !{{{
   forall( k=1:nrr) xk(k) = cos( (k-1)*Pi/(nrr-1) ) ! this way xk(1) = -1
   rr = xk*aa_r + bb_r         ! rr(1) = rmax !!! 

   call get_gaussLegendre_abs(nth,nph, th_GL)
   do i=1,nth; do j=1,nph
     
     !th(i,j) = (i-0.5d0)*pi/nth     ! equal-angular  quadrature lmax < nth/2 
      th(i,j) = th_GL(i)             ! Gauss-Legendre quadrature lmax < nth 
      ph(i,j) = (j-1.0d0)*2d0*pi/nph

      nx(i,j)  = cos(ph(i,j))*sin(th(i,j))
      ny(i,j)  = sin(ph(i,j))*sin(th(i,j))
      nz(i,j)  = cos(th(i,j))
         
!     ni = (/ nx(i,j), ny(i,j), nz(i,j) /)

   end do; end do
   ! }}}

   ! More initialization ! {{{
   open (unit=99,file=outfilename,status='replace',action='write')

   ! Set list with 10 points in the radial interval (for output)
   forall (i=1:10) ind_out(i) = (i - 1)*(nrr - 1)/9 + 1

   ! Set index arrays n(l,m) <-> l(n), m(n) 
   do l=0,lmax; do m=-l,l
      nf(l,m) = l**2 + l + m + 1  ! nmax = (lmax+1)**2
      lf(nf(l,m)) = l
      mf(nf(l,m)) = m
   end do; end do

   ! Tabulate spherical harmonics and Chebyshev polynomials
   call tabulate_Yrlm(lmax, nth,nph) 
   call tabulate_dYrlm(lmax, nth,nph) 
   call init_chebyshev(nrr)
  !call compute_Nj (lmax,Nullj )

  !! Code testing, compute num. error of test function / derivatives
  !call test_spectral( num_acc, check_sum )
  !print *, "Spectral accuracy, lmax   : ", num_acc, lmax
  !print *, "Check sum                 : ", check_sum 

   call init_metric("Kerr",spin,mass,rtrans,rmax) 
   !}}}

   ! Set initial data {{{

   vars0(psi,:,:,:)  = 0d0
   vars0(rho,:,:,:)  = 0d0
   vars0(psix,:,:,:) = 0d0
   vars0(psiy,:,:,:) = 0d0
   vars0(psiz,:,:,:) = 0d0

  !! Gaussian xyz
  !amp = 0.1d0; sig = 5d0; x0 = 10d0; y0 = 0; z0 = 0;
  !amp = 1d0; sig = 10d0; x0 = 2.5d0; y0 = 3.045d0; z0 = 1.7d0;
  !amp = 1d0; sig = 5d0; x0 = 1d0; y0 = 0d0; z0 = 0d0;
  !forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(rho,i,j,k) = amp * exp( - ( (rr(i)*nx(j,k) - x0)**2 + (rr(i)*ny(j,k) - y0)**2 + (rr(i)*nz(j,k) - z0)**2 )/(2d0*sig**2) )*omega(i) 

  !! Gaussian radial 
  !amp = 1d0; sig = 3d0; r0 = 7d0 
   amp = 1d0; sig = 1d0; r0 = 1d0 
   forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(rho,i,j,k) = amp * exp( - (rr(i) - r0)**2 /(sig**2) )

   ! Y^00 initial data, non-stationary 
  !amp = 1d0; sig = 10d0; r0 = 5d0 
  !forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(rho,i,j,k) = amp * exp( - (rr(i) - r0)**2 /(2d0*sig**2) )

   ! Y^00 initial data, stationary 
  !amp = 1d0; sig = 1d0; r0 = 1d0 
  !forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(psi,i,j,k) = amp * exp( - (rr(i) - r0)**2 /(2d0*sig**2) ) !*Yrlm_tb(nf(0,0),j,k) 

  !forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(psix,i,j,k) = - vars0(psi,i,j,k)* (rr(i) - r0) / sig**2 * nx(j,k)  
  !forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(psiy,i,j,k) = - vars0(psi,i,j,k)* (rr(i) - r0) / sig**2 * ny(j,k)  
  !forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(psiz,i,j,k) = - vars0(psi,i,j,k)* (rr(i) - r0) / sig**2 * nz(j,k)  

   ! Y^10 initial data 
!  amp = 1d0; sig = 1d0; r0 = 1d0 
!  forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(rho,i,j,k) = amp * exp( - (rr(i) - r0)**2 /(2d0*sig**2) )*Yrlm_tb(nf(1,0),j,k) 

!  ! Y^20 initial data 
!  amp = 1d0; sig = 1d0; r0 = 1d0 
!  forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(rho,i,j,k) = amp * exp( - (rr(i) - r0)**2 /(2d0*sig**2) )*Yrlm_tb(nf(2,0),j,k) 

!  amp = 1d0; sig = 2d0; r0 = 3d0 
!  forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(rho,i,j,k) = amp * exp( - (rr(i) - r0)**2 /(sig**2) )*Yrlm_tb(nf(4,0),j,k) 

!  ! Y^40 initial data 
  !amp = 1d0; sig = 1d0; r0 = 1d0 
  !forall ( i=1:nrr, j=1:nth, k=1:nph ) vars0(rho,i,j,k) = amp * exp( - (rr(i) - r0)**2 /(2d0*sig**2) )*Yrlm_tb(nf(4,0),j,k) 

   call test_acc_fct( vars0(rho,:,:,:), tmp )
   print *, "Num. accuracy initial data: ", tmp

   ! Mode cleaning initial data
   call fct_to_coeff( vars0(rho,:,:,:), ank_var )
   call coeff_to_fct( ank_var, vars0(rho,:,:,:) )
   ! }}}

   ! Main loop, time evolution  !{{{
   h  = dtime ! = delta( time ) < CFL*delta( space )
   h2 = h/2d0 
   h6 = h/6d0

   vars = vars0 ! initialize evolution variables 
   do it=1,itmax

     !! Pre/Post-step 
     !if ( it == 1 .or. mod(it,1) == 0 ) then

     !end if

      ! RK4 integration
      call rhs_scalar_wave_Kerr(vars         , k1 ) 
      call rhs_scalar_wave_Kerr(vars + h2*k1 , k2 )
      call rhs_scalar_wave_Kerr(vars + h2*k2 , k3 )
      call rhs_scalar_wave_Kerr(vars + h* k3 , k4 )
      !$OMP PARALLEL WORKSHARE
      vars = vars + h6*( k1+2d0*(k2+k3)+k4 ) 
      !$OMP END PARALLEL WORKSHARE

      ! Output to screen
      if ( it == 1 .or. mod(it,out_every) == 0 ) then

!        call fct_to_coeff( vars(rho,:,:,:), ank_var )
         
         print '(A,I6,7G18.10)', "it, T, max/min/ave psi: ", &
         it, it*h, maxval(vars(psi,:,:,:)), minval(vars(psi,:,:,:)), sum(vars(psi,:,:,:))/(nrr*nth*nph)

      end if

      ! Output to file
      if ( it == 1 .or. mod(it,out_every) == 0 ) then

         ank_var = 0
         call fct_to_coeff ( vars(psi,:,:,:), ank_var )
         call coeff_to_dfct( ank_var, dpsi )

         ! Compute constraint violation
         ! tmp = sum( vars(psix,:,:,:) - dpsi(1,:,:,:) + vars(psiy,:,:,:) - dpsi(2,:,:,:) + vars(psiz,:,:,:) - dpsi(3,:,:,:) )   
         call fct_to_coeff( ( vars(psix,:,:,:) - dpsi(1,:,:,:) )**2 + ( vars(psiy,:,:,:) - dpsi(2,:,:,:) )**2 + ( vars(psiz,:,:,:) - dpsi(3,:,:,:) )**2, ank_tmp )
         tmp = sqrt( ank_tmp( 1,1 ) ) 

         ! Test spectral accuracy
         call test_acc_fct( k1(3,:,:,:), tmp2 )
   
         ! Integrate psi^2 over full volume, to check convergence
         call fct_to_coeff( vars(psi,:,:,:)**2, ank_tmp )
         tmp3 = sqrt( ank_tmp(1,1) )

        !! Integrate psi^2 in radial direction at theta=Pi/2, phi=0
        !ank_tmp(1,:) = matmul( Tnk, vars(psi,:,(nth+1)/2, 1)**2 )
        !tmp4 = sqrt( ank_tmp(1,1) )

         ! Integrate psi^2 over sphere at r = (scri-horizon)/2 
         tmp5 = sqrt( sum( wYrlm_tb(1,:,:)*vars(psi,(5-1)*(nrr-1)/9+1,:,:)**2 ) )

         forall ( l=0:lmax ) lnorm(l) = sqrt( sum( ank_var(nf(l,-l):nf(l,l), :)**2 ) )

         !$OMP PARALLEL WORKSHARE
         forall (n=1:nmax,i=1:nrr) ank_tmp2(n,i) = sum( wYrlm_tb(n,:,:)*vars(psi,i,:,:) )
        !forall (n=1:nmax,i=1:nrr) ank_tmp(n,i)  = sum( wYrlm_tb(n,:,:)*vars(rho,i,:,:) )
         !$OMP END PARALLEL WORKSHARE

         formstr = "(141e18.10)"
         ! Output format: 
         ! 1.time, 2.-11. psi at 10 points, 12.-21. dpsi/dt at 10 points,
         ! 22.-81. Y^lm-coefficients of psi for m=0 l=0,1,2,3,4,5 at 10 points
         ! 82. contraint violation, 83. spectral accuracy, 84. L2-norm psi (volume), 85. L2-norm psi (fixed radius)
         write(99,formstr) it*h, vars(psi, ind_out, (nth+1)/2, 1), vars(rho, ind_out, (nth+1)/2, 1), &
                     ank_tmp2( nf(0,0), ind_out ), ank_tmp2( nf(1,0), ind_out ), ank_tmp2( nf(2,0), ind_out ), &
                     ank_tmp2( nf(3,0), ind_out ), ank_tmp2( nf(4,0), ind_out ), ank_tmp2( nf(5,0), ind_out ), & 
                    !ank_tmp ( nf(0,0), ind_out ), ank_tmp ( nf(1,0), ind_out ), ank_tmp ( nf(2,0), ind_out ), &
                    !ank_tmp ( nf(3,0), ind_out ), ank_tmp ( nf(4,0), ind_out ), ank_tmp ( nf(5,0), ind_out ), & 
                    !lnorm,& 
                     tmp, tmp2, tmp3, tmp5 !, tmp4
      end if

      ! Exit
      if ( it*h > t_final ) exit

   end do
   !}}}

end subroutine main

subroutine rhs_scalar_wave_Kerr(fct,df) !{{{
   ! Compute r.h.s. of 1st order reduced scalar wave eq. on Kerr background

   implicit none
   real (kind=8), intent(in)  :: fct (nvars,nrr,nth,nph)
   real (kind=8), intent(out) :: df  (nvars,nrr,nth,nph)

   real (kind=8)              :: f (nvars,nrr,nth,nph)
   real (kind=8)              :: ci(3,nrr,nth,nph)

   real (kind=8)              :: ank_fct(nmax,nrr)
   real (kind=8)              :: dx(nvars,3,nrr,nth,nph)

   real (kind=8)              :: damp 

   integer :: i,j,k,l,m,n, a,b,c
   integer :: lf 

   ank_fct = 0

   ! Compute spatial derivatives of psi, rho, psix, psiy, psiz
   do i=2,nvars
      call fct_to_coeff ( fct(i,:,:,:), ank_fct ) 
      call coeff_to_dfct( ank_fct, dx(i,:,:,:,:) ) 
   end do

   ! Symmetries mixed derivates, this fixes 3 of the 4 constraints
   ! with his step filtering of higher modes of psix, psiy, psiz not necessary
   !$OMP PARALLEL WORKSHARE
   dx(psix,2,:,:,:) = 0.5d0*( dx(psix,2,:,:,:) + dx(psiy,1,:,:,:) )
   dx(psix,3,:,:,:) = 0.5d0*( dx(psix,3,:,:,:) + dx(psiz,1,:,:,:) )
   dx(psiy,3,:,:,:) = 0.5d0*( dx(psiy,3,:,:,:) + dx(psiz,2,:,:,:) )
   !$OMP END PARALLEL WORKSHARE

   !$OMP PARALLEL WORKSHARE
   df(psi,:,:,:)  = fct(rho,:,:,:) 

   df(psix,:,:,:) = dx(rho,1,:,:,:) ! + ci(1,:,:,:) 
   df(psiy,:,:,:) = dx(rho,2,:,:,:) ! + ci(2,:,:,:) 
   df(psiz,:,:,:) = dx(rho,3,:,:,:) ! + ci(3,:,:,:) 

   df(rho,:,:,:) = - 1d0/gi(0,0,:,:,:) *(  2d0*( gi(0,1,:,:,:)*dx(rho,1,:,:,:) + gi(0,2,:,:,:)*dx(rho,2,:,:,:) + gi(0,3,:,:,:)*dx(rho,3,:,:,:) ) &
                   + gi(1,1,:,:,:)*dx(psix,1,:,:,:) + gi(2,2,:,:,:)*dx(psiy,2,:,:,:) + gi(3,3,:,:,:)*dx(psiz,3,:,:,:) &
                   + 2d0*( gi(1,2,:,:,:)*dx(psix,2,:,:,:) + gi(1,3,:,:,:)*dx(psix,3,:,:,:) + gi(2,3,:,:,:)*dx(psiy,3,:,:,:) ) &
                   - ( gam(0,:,:,:)*fct(rho,:,:,:) + gam(1,:,:,:)*fct(psix,:,:,:) + gam(2,:,:,:)*fct(psiy,:,:,:) + gam(3,:,:,:)*fct(psiz,:,:,:) ) &
                   - 1d0/6d0 * Ric*fct(psi,:,:,:) ) 
   !$OMP END PARALLEL WORKSHARE

   ! Spectral filtering, got the best results for filtering psi and rho at lmax
   ! the error stays constant around machine eps
   call fct_to_coeff( df(psi,:,:,:), ank_fct ) 
   lf = lmax
   ank_fct( nf(lf,-lf): , : ) = 0
   call coeff_to_fct( ank_fct, df(psi,:,:,:) )

   call fct_to_coeff( df(rho,:,:,:), ank_fct ) 
   lf = lmax 
   ank_fct( nf(lf,-lf): , : ) = 0
   call coeff_to_fct( ank_fct, df(rho,:,:,:) )

end subroutine rhs_scalar_wave_Kerr !}}}

subroutine init_metric( metric_type, spin, mass, rtrans, rmaxi ) !{{{
   ! Compute 4-metric, Gammas, Ricci scalar
   implicit none

   ! Define variables {{{
   character (len=*), intent(in) :: metric_type
   real (kind=8), intent(in)     :: spin, mass, rtrans, rmaxi

   integer   :: i, j, k
   integer   :: a,b,c
 
   real (kind=8) :: eta(4,4), del3(2:4,2:4)

   real (kind=8) :: wid 

   real (kind=8) :: xx, yy, zz, rr0
   real (kind=8) :: nn(3) 
   real (kind=8) :: tmp, tmp2

   real (kind=8) :: tf, dtf, ddtf 
   real (kind=8) :: om, dom, ddom
   real (kind=8) :: drno, omD, omD_om, omD_om2, omDr, omDr_om, omomDr_om2, omomDr_om, omomDr, omDDr_omD_om2
   real (kind=8) :: hD, hDD_om2

   real (kind=8) :: ss_om2, rBL_om, VV, VV_om, nl, Vnl2
   real (kind=8) :: ll(4), llN(4), llN_om(4), nnN(4), etaN_om2(4,4)  
   real (kind=8) :: VVs, nls
   real (kind=8) :: Kp, Km, I1 
   real (kind=8) :: Vap, VapD, VapD_om2, L0, L0D, cout, coutD, Hc, HcD, Gc

   real (kind=8) :: C_KS(4), om_C(4), Hess(4), gammas(4)
   real (kind=8) :: inf_inf(4)

   real (kind=8) :: xn, yn, zn, rn, dxh_om, ddxh_om, drdr, ddrdr, gi0(0:3,0:3), Jno(3,3), dJno(3,3,3), Hess2(3,3,3), Hht(3) 
 
   character :: msg*100
   ! }}}
 
   eta(1,:) = (/ -1d0, 0d0, 0d0, 0d0 /);  del3(2,:) = (/ 1d0, 0d0, 0d0 /) 
   eta(2,:) = (/  0d0, 1d0, 0d0, 0d0 /);  del3(3,:) = (/ 0d0, 1d0, 0d0 /) 
   eta(3,:) = (/  0d0, 0d0, 1d0, 0d0 /);  del3(4,:) = (/ 0d0, 0d0, 1d0 /) 
   eta(4,:) = (/  0d0, 0d0, 0d0, 1d0 /);

   wid = rmaxi - rtrans

   if (metric_type=='Kerr') then

   ! Kerr metric on hyperboloidal slicing, xx, yy, zz, rr are the rescaled coords !{{{
   do k=1,nph
      do j=1,nth
         do i=1,nrr
 
            xx = rr(i)*nx(j,k) ! x(i,j,k)
            yy = rr(i)*ny(j,k) ! y(i,j,k)
            zz = rr(i)*nz(j,k) ! z(i,j,k)
            rr0 = rr(i)         ! r(i,j,k)

            nn(1) = nx(j,k) ! xx/rr
            nn(2) = ny(j,k) ! yy/rr
            nn(3) = nz(j,k) ! zz/rr
 
            ! Set conformal factor (omega) / transition function (tf) and derivatives {{{
            tmp = (rr0-rtrans) 
 
           !! 4th order polynomial transition function  
           !tf   =      (tmp/wid)**4
           !dtf  =  4d0*tmp**3 / wid**4
           !ddtf = 12d0*tmp**2 / wid**4
 
           !! 6th order polynomial transition function  
           !tf   =          tmp**6 / wid**6
           !dtf  =  6d0    *tmp**5 / wid**6
           !ddtf =  6d0*5d0*tmp**4 / wid**6

           !! 8th order polynomial transition function  
           !tf   =          tmp**8 / wid**8
           !dtf  =  8d0    *tmp**7 / wid**8
           !ddtf =  8d0*7d0*tmp**6 / wid**8

            ! 10th order polynomial transition function  
            tf   =          tmp**10 / wid**10
            dtf  =  10d0    *tmp**9 / wid**10
            ddtf =  10d0*9d0*tmp**8 / wid**10
 
            ! Define conformal factor and derivatives
            if (rr0<rtrans) then ! Kerr0-Schild region
               om   = 1d0
               dom  = 0d0
               ddom = 0d0
            else                 ! rescaled 'unphysical' region
               om   = 1d0 - tf
               dom  = - dtf
               ddom = - ddtf
            end if
   
            omega(i) = om 

            ! Compute derivatives wrt old radius expressed in new coords 
            L0         = 1d0/( om - rr0*dom ) ! as definition in jasiulek-1109.2513 
            L0D        = L0**3 * om**2 * rr0 * ddom ! L'(r_old)  
            drno       = om**2      *L0       ! r_new'(r_old) 
            omD        = om**2  *dom*L0       ! Om'(r_old) 
            omD_om     = om     *dom*L0       ! Om'(r_old)/Om
            omD_om2    =         dom*L0       ! Om'(r_old)/Om^2
            omDr       = om*rr0 *dom*L0       ! Om'(r_old)*r_old 
            omDr_om    =    rr0 *dom*L0       ! Om'(r_old)*r_old / Om 
            omomDr_om2 =             L0       ! (Om + Om'(ro)*ro ) / Om^2
            omomDr_om  =          om*L0       ! (Om + Om'(ro)*ro ) / Om
            omomDr     = drno                 ! (Om + Om'(ro)*ro )
            
            omDDr_omD_om2 =(ddom*om**2 *rr0 - dom*(om - 3d0*dom*rr0)*(om - dom*rr0))/(om - dom*rr0)**3   ! ( Om''(ro)*ro - Om'(ro) ) / Om^2 
            !}}}
   
            ! Define height-function derivatives h'(ro), h''(ro) (wrt old radius) !{{{ 
            if (rr0<rtrans) then    ! Kerr0-Schild region
               hD   = 0
            else                    ! rescaled region
               ! v1: hD-simple, simple height function that is 'well' repre. in Cheb. coeff. space
               hD   = (1d0+4d0*mass*om/rr0)*( 1d0-om**2 *L0 ) 
               hDD_om2  = -4d0*mass/rr0**2 *(1d0-om**2 *L0) - (1d0+4d0*mass*om/rr0)*( 2d0 *omD_om*L0 + L0D )
 
            !  ! v2: as in jasiulek-1109, allows control of coordinate speed
            !  Vap  =   mass*om / rr0                                          ! an approximation to VV = rBL/ss
            !  VapD = - mass* ( om/rr0 )**2                                    ! Vap'(r_old)    
            !  VapD_om2 = - mass* ( 1d0/rr0 )**2                               ! Vap'(r_old) / Om^2

            !  L0D  = L0**3 * om**2 * rr0 * ddom                               ! L'(r_old)  

            !  cout = (1d0 - 2d0*mass / rr0) / (1d0 + 2d0*mass / rr0)          ! set coordinate speed   
            !  coutD= 4d0* mass*drno / ( 2d0*mass + rr0 )**2    

            !  Hc   = (1d0 - 2d0*Vap)/(1d0 + 2d0*Vap) / cout 
            !  HcD  = -4d0*VapD / ( 1d0 + 2d0*Vap )**2 / cout - (1d0 - 2d0*Vap)/(1d0 + 2d0*Vap) / cout**2 *coutD

            !  hD   = (1d0 + 2d0*Vap)/(1d0 - 2d0*Vap) * ( 1d0 - om**2 *L0*Hc ) 

            ! !hDD  = 4d0*VapD / ( 1d0 - 2d0*Vap )**2 * ( 1d0 - om**2 *L0*Hc ) + &
            ! !       (1d0 + 2d0*Vap)/(1d0 - 2d0*Vap) * ( -1d0)*( 2d0*om*omD*L0*Hc + om**2 *L0D*Hc + om**2 *L0*HcD )

            !  hDD_om2 = 4d0*VapD_om2 / ( 1d0 - 2d0*Vap )**2 * ( 1d0 - om**2 *L0*Hc ) + &
            !         (1d0 + 2d0*Vap)/(1d0 - 2d0*Vap) * ( -1d0)*( 2d0*omD_om*L0*Hc + L0D*Hc + L0*HcD )
 
            end if !}}}
 
            ! Compute metric ! {{{
            ! Compute in-going principal null normal 
            ss_om2   = sqrt( ( rr0**2 - (om*spin)**2 )**2 + ( 2d0*spin*om *zz )**2 ) ! ss_om2   = Om^2 *ss
            rBL_om   = sqrt( rr0**2 - (om*spin)**2 + ss_om2 )/sqrt(2d0)              ! rBL_om  = Om *rBL 
            VV       = mass *om *rBL_om/ss_om2                                         
            VV_om    = mass *    rBL_om/ss_om2                                       ! VV_om    = VV / Om 
 
            ll(1) = -1d0
            ll(2) = (rBL_om*xx + om*spin*yy) / (rBL_om**2 + (om*spin)**2)
            ll(3) = (rBL_om*yy - om*spin*xx) / (rBL_om**2 + (om*spin)**2)
            ll(4) = zz / rBL_om
 
            nl   = rBL_om/rr0          ! n^j l_j
            Vnl2 = 2d0*VV* nl**2       ! 2*V*(n^j l_j)^2
 
            ! eta_new/Om^2
            etaN_om2(1,1:4) = (/   (-1d0+hD**2)/om**2,                -hD*omomDr_om2*nn(1),                -hD*omomDr_om2*nn(2),                -hD*omomDr_om2*nn(3)/) 
            etaN_om2(2,1:4) = (/ -hD*omomDr_om2*nn(1), 1d0+omDr_om*(2d0+omDr_om)*nn(1)*nn(1),     omDr_om*(2d0+omDr_om)*nn(2)*nn(1),     omDr_om*(2d0+omDr_om)*nn(3)*nn(1)/) 
            etaN_om2(3,1:4) = (/ -hD*omomDr_om2*nn(2),     omDr_om*(2d0+omDr_om)*nn(1)*nn(2), 1d0+omDr_om*(2d0+omDr_om)*nn(2)*nn(2),     omDr_om*(2d0+omDr_om)*nn(3)*nn(2)/) 
            etaN_om2(4,1:4) = (/ -hD*omomDr_om2*nn(3),     omDr_om*(2d0+omDr_om)*nn(1)*nn(3),     omDr_om*(2d0+omDr_om)*nn(2)*nn(3), 1d0+omDr_om*(2d0+omDr_om)*nn(3)*nn(3)/) 
 
            llN    = (/ ll(1)-hD*nl, om*ll(2:4)+omDr*nn(1:3)*nl /)
            llN_om = (/   llN(1)/om, ll(2:4)+omDr_om*nn(1:3)*nl /)
 
            ! Compute metric component gi00 !{{{ 
            ! v1: g00 compatible with hD-simple, simple height function that is 'well' repre. in Cheb. coeff. space
            if ( om/rr0 > 0.01 ) then  ! off scri+
               gi(0,0,i,j,k) = etaN_om2(1,1) - 2d0*VV_om *llN(1)*llN(1) / om  
            else                       ! around scri+
              !tmp  = (-1d0+hD**2 - 2d0*VV*( 1d0+hD*nl )**2 )/om**2
               tmp2 = L0**2 *om**2 *(1d0 + (4d0*mass*om)/rr0)**2*(1d0 - 2d0*nl**2*VV) + L0*(1d0 + (4d0*mass*om)/rr0)*(4d0*nl*VV + &
                      (1d0 + (4d0*mass*om)/rr0)*(-2d0 + 4d0*nl**2*VV))
 
              !VVs = (rr0/om)**3  *(mass*om/rr0 - VV) ! below we expand this in om/rr0 around om/rr0 = 0 (scri+)
              !nls = (rr0/om)**2  *(1d0-nl)
 
               VVs = (mass*(-1d0 + 3d0*nn(3)**2)*spin**2)/2d0 + (om**2*(-(mass*(3d0 - 30d0*nn(3)**2 + 35d0*nn(3)**4)*spin**4)/8d0 + & 
                     (om**2*((mass*(-5d0 + 21d0*nn(3)**2*(5d0 - 15d0*nn(3)**2 + 11d0*nn(3)**4))*spin**6)/16d0 + &
                     (om**2*(-(mass*(35d0 - 1260d0*nn(3)**2 + 6930d0*nn(3)**4 - 12012d0*nn(3)**6 + 6435d0*nn(3)**8)*spin**8)/128d0 + &
                     (mass*(-63d0 + 11d0*nn(3)**2*(315d0 + 13d0*nn(3)**2*(-210d0 + 630d0*nn(3)**2 - 765d0*nn(3)**4 + &
                     323d0*nn(3)**6)))*om**2*spin**10)/(256d0*rr0**2)))/rr0**2))/rr0**2))/rr0**2
 
               nls = -((-1d0 + nn(3)**2)*spin**2)/2d0 + (om**2*(((1d0 - 6d0*nn(3)**2 + 5d0*nn(3)**4)*spin**4)/8d0 + &
                      (om**2*(((1d0 - 15d0*nn(3)**2 + 35d0*nn(3)**4 - 21d0*nn(3)**6)*spin**6)/16d0 +                        &
                      (om**2*(2d0*(5d0 - 140d0*nn(3)**2 + 630d0*nn(3)**4 - 924d0*nn(3)**6 + 429d0*nn(3)**8)*spin**8 -        &
                      ((-1d0 + nn(3))*(1d0 + nn(3))*(7d0 + 11d0*nn(3)**2*(-28d0 + 182d0*nn(3)**2 - 364d0*nn(3)**4 + &
                      221d0*nn(3)**6))*om**2*spin**10)/rr0**2))/(256d0*rr0**2)))/rr0**2))/rr0**2
 
               gi(0,0,i,j,k) = tmp2 + 1d0/rr0**2 *( - 16d0*mass**2 + (-2d0*mass*om*rr0**2*(nls*rr0**2*(nls*om**2 - 4d0*rr0**2) + & 
                               8d0*mass*nls*om*rr0*(nls*om**2 - 3d0*rr0**2) + 16d0*mass**2*(-(nls*om**2) + rr0**2)**2) + &
                               2d0*om*(4d0*mass*nls*om**3 + nls*om**2*rr0 - 4d0*mass*om*rr0**2 - 2d0*rr0**3)**2*VVs)/rr0**7  )
      
            end if

           !! v2: g00 as in jasiulek-1109, allows control of coordinate speed
           !I1 = sqrt( 1d0 + 2d0*VV*( 1d0 - nl**2 ) ) 
           !Kp = ( + I1 + 2d0*nl*VV )/( 1d0 - Vnl2 )   
           !Km = ( - I1 + 2d0*nl*VV )/( 1d0 - Vnl2 )   

           !if ( om/rr0 > 0.01 ) then
           !   gi(0,0,i,j,k) = ( 1d0 - Vnl2 )*( hD - Km )*( hD - Kp )/om**2  
           !else 
           !   ! close to scri+ expand Gc in 1/r_old using e.g. Mathematica
           !   ! (1-2V)/(1+2V) Kp = 1 + 1/(r_old)^2 *Gc as in jasiulek-1109
           !   ! Gc = ( (1d0 - 2d0*Vap)/(1d0 + 2d0*Vap)* Kp - 1d0 )* (rr0/om)**2
           !   tmp = om/rr0  ! 1 / r_old 
           !   Gc = tmp*(-4d0*spin**2*mass*nn(3)**2 + tmp*(-4d0*spin**2*mass**2 + 4d0*spin**2*mass**2*nn(3)**2 + tmp*((spin**4*mass)/4d0 - (17d0*spin**4*mass*nn(3)**2)/2d0 - &
           !        16d0*spin**2*mass**3*nn(3)**2 + (49d0*spin**4*mass*nn(3)**4)/4d0 + tmp*(-2d0*spin**4*mass**2 - 16d0*spin**2*mass**4 + 16d0*spin**4*mass**2*nn(3)**2 + &
           !        16d0*spin**2*mass**4*nn(3)**2 - 6d0*spin**4*mass**2*nn(3)**4 + tmp*((spin**6*mass)/4d0 + 6d0*spin**4*mass**3 - (27d0*spin**6*mass*nn(3)**2)/2d0 - &
           !        28d0*spin**4*mass**3*nn(3)**2 - 64d0*spin**2*mass**5*nn(3)**2 + (201d0*spin**6*mass*nn(3)**4)/4d0 + 54d0*spin**4*mass**3*nn(3)**4 - &
           !        41d0*spin**6*mass*nn(3)**6 + tmp*((-7d0*spin**6*mass**2)/4d0 - 64d0*spin**2*mass**6 + (129d0*spin**6*mass**2*nn(3)**2)/4d0 + &
           !        80d0*spin**4*mass**4*nn(3)**2 + 64d0*spin**2*mass**6*nn(3)**2 - (205d0*spin**6*mass**2*nn(3)**4)/4d0 - 16d0*spin**4*mass**4*nn(3)**4 + &
           !        (19d0*spin**6*mass**2*nn(3)**6)/4d0 + ((15d0*spin**8*mass)/64d0 + 5d0*spin**6*mass**3 + 40d0*spin**4*mass**5 - &
           !        (303d0*spin**8*mass*nn(3)**2)/16d0 - 53d0*spin**6*mass**3*nn(3)**2 - 80d0*spin**4*mass**5*nn(3)**2 - 256d0*spin**2*mass**7*nn(3)**2 + &
           !        (4125d0*spin**8*mass*nn(3)**4)/32d0 + 187d0*spin**6*mass**3*nn(3)**4 + 232d0*spin**4*mass**5*nn(3)**4 - (3999d0*spin**8*mass*nn(3)**6)/16d0 - &
           !        203d0*spin**6*mass**3*nn(3)**6 + (9199d0*spin**8*mass*nn(3)**8)/64d0)*tmp))))))

           !   gi(0,0,i,j,k) = ( 1d0 - Vnl2 )*( hD - Km )*( -Hc*L0 - Gc/rr0**2 )*( 1d0 + 2d0*Vap ) / ( 1d0 - 2d0*Vap )  
           !end if !}}} 

            ! Compute remaining com. of inverse metric
            gi(0,1,i,j,k) = etaN_om2(1,2) - 2d0*VV_om *llN(1)*llN_om(2)   
            gi(0,2,i,j,k) = etaN_om2(1,3) - 2d0*VV_om *llN(1)*llN_om(3)   
            gi(0,3,i,j,k) = etaN_om2(1,4) - 2d0*VV_om *llN(1)*llN_om(4)   
 
            gi(1,1,i,j,k) = etaN_om2(2,2) - 2d0*VV_om *llN(2)*llN_om(2) 
            gi(2,2,i,j,k) = etaN_om2(3,3) - 2d0*VV_om *llN(3)*llN_om(3) 
            gi(3,3,i,j,k) = etaN_om2(4,4) - 2d0*VV_om *llN(4)*llN_om(4) 
            gi(1,2,i,j,k) = etaN_om2(2,3) - 2d0*VV_om *llN(2)*llN_om(3) 
            gi(1,3,i,j,k) = etaN_om2(2,4) - 2d0*VV_om *llN(2)*llN_om(4) 
            gi(2,3,i,j,k) = etaN_om2(3,4) - 2d0*VV_om *llN(3)*llN_om(4) 

            gi(1,0,i,j,k) = gi(0,1,i,j,k); gi(2,0,i,j,k) = gi(0,2,i,j,k); gi(3,0,i,j,k) = gi(0,3,i,j,k) 
            gi(2,1,i,j,k) = gi(1,2,i,j,k); gi(3,1,i,j,k) = gi(1,3,i,j,k); gi(3,2,i,j,k) = gi(2,3,i,j,k) 
            !}}}
 
          ! ! Compute determinate
          ! sqrtdet4(i,j,k) = abs( 1d0/omomDr_om2 )
 
            ! Compute scalar Curvature for rescaled metric
            ric(i,j,k)  = -6d0 *( (1d0 - Vnl2)*omDDr_omD_om2 / rr0 + omD_om2/rr0 *(3d0 - 2d0*VV) - 2d0*omD_om*mass*nl/ss_om2 )

            ! Compute Gammas !{{{
           !nnN_om = (/ -hD_om, nn(1:3)* omomDr_om /) 
   
            ! Set Gammas for hyperboloidal slicings
            C_KS = llN*2d0*mass/ss_om2                  ! Gamma as in KS-coords
   
          ! ! Gamma due to conformal rescaling
          ! om_C = -2d0*omD_om/om**2  *( nnN - 2d0*VV*nl* llN  )  
          ! ! Hessian^a_bc g^bc = d^2 xn^a/(dxo^b dxo^c) g^bc
          ! Hess = 1d0/om**2  *(/ -hDD*(1d0-Vnl2) + 2d0*hD*om/rr*VV*(1d0-nl**2) - 2d0*hD*om/rr, & 
          !           nn*( omDDr_omD*( 1d0 - Vnl2 ) + omD*( 5d0 - 2d0*VV ) ) - 2d0*ll(2:4)*omD*2d0*VV*nl /)
   
            ! Omit singular pieces
            ! Gamma(resc.)^b + (/ 2d0*omD_om/om**2 *( -hD ) , 0, 0, 0 /)
            om_C = -2d0*omD_om2 *( (/ 0d0, nn(1:3)* omomDr_om /) - 2d0*VV_om*nl* llN  )  
            ! Hessian^b + (/ 2d0*hD*om/rr, 0, 0, 0 /)   
            Hess = (/ -hDD_om2*(1d0-Vnl2) + 2d0*hD/rr0*VV_om*(1d0-nl**2), & 
                      nn*( omDDr_omD_om2*( 1d0 - Vnl2 ) + omD_om2*( 5d0 - 2d0*VV ) ) - 2d0*ll(2:4)*omD_om2*2d0*VV*nl /)
   
            ! Cancel singular pieces of om_C and -Hess 
           !inf_inf = -2d0*omD_om/om**2 (/ nnN(1), 0,0,0/) - 1d0/om**2 *(/ -2d0*hd*om/rr0, 0,0,0/)  
            inf_inf = (/ 2d0*hD *omomDr_om2/rr0, 0d0, 0d0, 0d0 /) ! 2*hD *( om+omD*ro)/(om**3 *ro) 
   
            gammas  = C_KS + om_C - Hess + inf_inf
   
            gam(0:3,i,j,k) = gammas(1:4)
            ! }}}
 
         end do
      end do
   end do !}}} 
   
   else if (metric_type/='Kerr') then

      print *, "Not yet supported!"

   end if
 
end subroutine init_metric !}}}

!  subroutine rhs_scalar_wave(f,df) !{{{
!  
!     implicit none
!     real (kind=8), intent(in)  :: f   (nvars,nrr,nth,nph)
!     real (kind=8), intent(out) :: df  (nvars,nrr,nth,nph)
!  
!     real (kind=8)              :: ank_fct(nmax,nrr)
!     real (kind=8)              :: dx(nvars,3,nrr,nth,nph)
!  
!     integer, parameter         :: psi = 1
!     integer, parameter         :: rho = 2 
!     integer :: i,j,l,m, a,b,c
!  
!     call fct_to_coeff( f(rho,:,:,:), ank_fct ) 
!     call coeff_to_dfct( ank_fct, dx(rho,:,:,:,:) ) 
!  
!     !$OMP PARALLEL WORKSHARE
!     df(psi,:,:,:) = f(rho,:,:,:) 
!     df(rho,:,:,:) = 0.5d0*( dx(rho,1,:,:,:) + dx(rho,2,:,:,:) + dx(rho,3,:,:,:) ) 
!     !$OMP END PARALLEL WORKSHARE
!  
!  !  call fct_to_coeff( dfct, ank_fct )
!  !  call coeff_to_fct( ank_fct, dfct )

!  end subroutine rhs_scalar_wave !}}}

subroutine fct_to_coeff( fct, ank_fct ) !{{{
   implicit none

   real (kind=8), intent(in)  :: fct(nrr,nth,nph)
   real (kind=8), intent(out) :: ank_fct(nmax,nrr)

   real (kind=8)              :: ank_tmp(nmax,nrr)
   integer                    :: n,i,j,k

   ! Transform fct into coeff-space
   !$OMP PARALLEL WORKSHARE
   forall (n=1:nmax,i=1:nrr) ank_tmp(n,i) = sum( wYrlm_tb(n,:,:)*fct(i,:,:) )
   forall (n=1:nmax)         ank_fct(n,:) = matmul( Tnk, ank_tmp(n,:) ) 
   !$OMP END PARALLEL WORKSHARE

end subroutine fct_to_coeff

subroutine coeff_to_fct( ank_fct, fct )
   implicit none

   real (kind=8), intent(in)  :: ank_fct(nmax,nrr)
   real (kind=8), intent(out) :: fct(nrr,nth,nph)

   real (kind=8)              :: ank_tmp(nmax,nrr)
   integer                    :: n,i,j,k

   ! Transform coeff into fct
   !$OMP PARALLEL WORKSHARE
   forall (n=1:nmax)                ank_tmp(n,:) = matmul( iTnk, ank_fct(n,:) )   
   forall (i=1:nrr,j=1:nth,k=1:nph) fct(i,j,k) = sum( ank_tmp(:,i)* Yrlm_tb(:,j,k) )
   !$OMP END PARALLEL WORKSHARE

end subroutine coeff_to_fct

subroutine coeff_to_dfct( ank_fct, dfct )
   implicit none

   real (kind=8), intent(in)  :: ank_fct(nmax,nrr)
   real (kind=8), intent(out) :: dfct(3,nrr,nth,nph)

   real (kind=8)              :: ank_tmp(nmax,nrr), dank_tmp(nmax,nrr)
   real (kind=8)              :: drTmp(nrr,nth,nph), dxiTmp(3,nrr,nth,nph) 
   integer                    :: n,i,j,k,  d

   ! Compute r-deriv 
   ! Compute coefficients of derivative 
   do n=1,nmax
      call coeff_to_dcoeff( ank_fct(n,:), dank_tmp(n,:), nrr )
   end do

   !$OMP PARALLEL WORKSHARE
  !forall (n=1:nmax)                ank_tmp(n,:) = matmul( diTnk, ank_fct(n,:) ) / aa_r ! alternative to the lines below
  !forall (i=1:nrr,j=1:nth,k=1:nph) drTmp(i,j,k) = sum( ank_tmp(:,i)* Yrlm_tb(:,j,k) )
   forall (n=1:nmax)                ank_tmp(n,:) = matmul( iTnk, dank_tmp(n,:) ) / aa_r
   forall (i=1:nrr,j=1:nth,k=1:nph) drTmp(i,j,k) = sum( ank_tmp(:,i)* Yrlm_tb(:,j,k) )

   ! Compute 1. x/y/z-derivative
   forall (n=1:nmax)                ank_tmp(n,:) = matmul( iTnk, ank_fct(n,:) ) 
   forall (i=1:nrr,j=1:nth,k=1:nph) dxiTmp(1,i,j,k) = sum( ank_tmp(:,i)* dxYrlm_tb(:,j,k) )
   forall (i=1:nrr,j=1:nth,k=1:nph) dxiTmp(2,i,j,k) = sum( ank_tmp(:,i)* dyYrlm_tb(:,j,k) )
   forall (i=1:nrr,j=1:nth,k=1:nph) dxiTmp(3,i,j,k) = sum( ank_tmp(:,i)* dzYrlm_tb(:,j,k) )

   ! Assemble
   forall ( i=1:nrr, j=1:nth, k=1:nph ) dfct(1,i,j,k) = nx(j,k)*drTmp(i,j,k) + dxiTmp(1,i,j,k)/rr(i)
   forall ( i=1:nrr, j=1:nth, k=1:nph ) dfct(2,i,j,k) = ny(j,k)*drTmp(i,j,k) + dxiTmp(2,i,j,k)/rr(i)
   forall ( i=1:nrr, j=1:nth, k=1:nph ) dfct(3,i,j,k) = nz(j,k)*drTmp(i,j,k) + dxiTmp(3,i,j,k)/rr(i)
   !$OMP END PARALLEL WORKSHARE

end subroutine coeff_to_dfct

subroutine coeff_to_ddfct( ank_fct, ddfct )
   implicit none

   real (kind=8), intent(in)  :: ank_fct(nmax,nrr)
   real (kind=8), intent(out) :: ddfct(3,3,nrr,nth,nph)

   real (kind=8)              :: ank_tmp(nmax,nrr), ank_tmp2(nmax,nrr)
   real (kind=8)              :: drTmp(nrr,nth,nph), dxiTmp(3,nrr,nth,nph) 
   real (kind=8)              :: ddrTmp(nrr,nth,nph), ddrxiTmp(3,nrr,nth,nph) 
   real (kind=8)              :: ddxijTmp(3,3,nrr,nth,nph) 
   integer                    :: n,i,j,k

   ! Transform coeff into 2. derivative of fct
   !$OMP PARALLEL WORKSHARE
   forall (n=1:nmax)                ank_tmp(n,:)  = matmul( diTnk, ank_fct(n,:) ) / aa_r
   forall (i=1:nrr,j=1:nth,k=1:nph) drTmp(i,j,k)  = sum( ank_tmp(:,i)*  Yrlm_tb(:,j,k) )

   forall (n=1:nmax)                ank_tmp2(n,:) = matmul( ddiTnk, ank_fct(n,:) ) / aa_r**2
   forall (i=1:nrr,j=1:nth,k=1:nph) ddrTmp(i,j,k) = sum( ank_tmp2(:,i)* Yrlm_tb(:,j,k) )

   forall (i=1:nrr,j=1:nth,k=1:nph) ddrxiTmp(1,i,j,k) = sum( ank_tmp(:,i)* dxYrlm_tb(:,j,k) )
   forall (i=1:nrr,j=1:nth,k=1:nph) ddrxiTmp(2,i,j,k) = sum( ank_tmp(:,i)* dyYrlm_tb(:,j,k) )
   forall (i=1:nrr,j=1:nth,k=1:nph) ddrxiTmp(3,i,j,k) = sum( ank_tmp(:,i)* dzYrlm_tb(:,j,k) )

   forall (n=1:nmax)                ank_tmp(n,:) = matmul( iTnk, ank_fct(n,:) ) 
   forall (i=1:nrr,j=1:nth,k=1:nph) ddxijTmp(1,1,i,j,k) = sum( ank_tmp(:,i)* dxxYrlm_tb(:,j,k) )
   forall (i=1:nrr,j=1:nth,k=1:nph) ddxijTmp(2,2,i,j,k) = sum( ank_tmp(:,i)* dyyYrlm_tb(:,j,k) )
   forall (i=1:nrr,j=1:nth,k=1:nph) ddxijTmp(3,3,i,j,k) = sum( ank_tmp(:,i)* dzzYrlm_tb(:,j,k) )

   forall (i=1:nrr,j=1:nth,k=1:nph) ddxijTmp(1,2,i,j,k) = sum( ank_tmp(:,i)* dxyYrlm_tb(:,j,k) )
   forall (i=1:nrr,j=1:nth,k=1:nph) ddxijTmp(1,3,i,j,k) = sum( ank_tmp(:,i)* dxzYrlm_tb(:,j,k) )
   forall (i=1:nrr,j=1:nth,k=1:nph) ddxijTmp(2,3,i,j,k) = sum( ank_tmp(:,i)* dyzYrlm_tb(:,j,k) )

   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddfct(1,1,i,j,k) = 1d0/rr(i) *( del3(1,1) - nx(j,k)*nx(j,k) )*drTmp(i,j,k) + nx(j,k)*( nx(j,k)*ddrTmp(i,j,k) + ddrxiTmp(1,i,j,k)/rr(i) ) + nx(j,k)*ddrxiTmp(1,i,j,k)/rr(i) + ddxijTmp(1,1,i,j,k)/rr(i)**2
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddfct(2,2,i,j,k) = 1d0/rr(i) *( del3(2,2) - ny(j,k)*ny(j,k) )*drTmp(i,j,k) + ny(j,k)*( ny(j,k)*ddrTmp(i,j,k) + ddrxiTmp(2,i,j,k)/rr(i) ) + ny(j,k)*ddrxiTmp(2,i,j,k)/rr(i) + ddxijTmp(2,2,i,j,k)/rr(i)**2
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddfct(3,3,i,j,k) = 1d0/rr(i) *( del3(3,3) - nz(j,k)*nz(j,k) )*drTmp(i,j,k) + nz(j,k)*( nz(j,k)*ddrTmp(i,j,k) + ddrxiTmp(3,i,j,k)/rr(i) ) + nz(j,k)*ddrxiTmp(3,i,j,k)/rr(i) + ddxijTmp(3,3,i,j,k)/rr(i)**2

   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddfct(1,2,i,j,k) = 1d0/rr(i) *( del3(1,2) - nx(j,k)*ny(j,k) )*drTmp(i,j,k) + nx(j,k)*( ny(j,k)*ddrTmp(i,j,k) + ddrxiTmp(2,i,j,k)/rr(i) ) + ny(j,k)*ddrxiTmp(1,i,j,k)/rr(i) + ddxijTmp(1,2,i,j,k)/rr(i)**2
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddfct(1,3,i,j,k) = 1d0/rr(i) *( del3(1,3) - nx(j,k)*nz(j,k) )*drTmp(i,j,k) + nx(j,k)*( nz(j,k)*ddrTmp(i,j,k) + ddrxiTmp(3,i,j,k)/rr(i) ) + nz(j,k)*ddrxiTmp(1,i,j,k)/rr(i) + ddxijTmp(1,3,i,j,k)/rr(i)**2
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddfct(2,3,i,j,k) = 1d0/rr(i) *( del3(2,3) - ny(j,k)*nz(j,k) )*drTmp(i,j,k) + ny(j,k)*( nz(j,k)*ddrTmp(i,j,k) + ddrxiTmp(3,i,j,k)/rr(i) ) + nz(j,k)*ddrxiTmp(2,i,j,k)/rr(i) + ddxijTmp(2,3,i,j,k)/rr(i)**2
   ddfct(2,1,:,:,:) = ddfct(1,2,:,:,:); ddfct(3,1,:,:,:) = ddfct(1,3,:,:,:); ddfct(3,2,:,:,:) = ddfct(2,3,:,:,:)
   !$OMP END PARALLEL WORKSHARE

end subroutine coeff_to_ddfct !}}}

subroutine test_acc_fct( fct, num_acc ) !{{{
   ! Compute num. trunction error of a function 

   implicit none

   real (kind=8), intent(in)  :: fct(nrr,nth,nph)
   real (kind=8), intent(out) :: num_acc

   real (kind=8) :: test(nrr,nth,nph)

   real (kind=8) :: ank_fct(1:nmax,1:nrr) 

   real (kind=8) :: tmax, t2max 

   integer       :: i,j,k, l,m,n
   integer       :: mx(3) 

   ! Transform test function forward/backward
   call fct_to_coeff( fct, ank_fct )
   call coeff_to_fct( ank_fct, test)

   ! Compute relative num. error
   mx    = maxloc( abs( test - fct )  ) 
   tmax  = fct(mx(1),mx(2),mx(3)) 
   t2max = test(mx(1),mx(2),mx(3)) 

   num_acc =  abs( tmax - t2max ) / tmax 

end subroutine test_acc_fct !}}}

subroutine test_spectral( num_acc, check_sum, dlevel ) !{{{
   ! Test spectral code by computing num. trunction error of a test function and
   ! its derivatives

   implicit none

   real (kind=8), intent(out) :: num_acc, check_sum
   character (len=*), optional, intent(in) :: dlevel 

   real (kind=8) :: test(nrr,nth,nph), test2(nrr,nth,nph)  

   real (kind=8) :: fct(nrr,nth,nph), dfct(3,nrr,nth,nph), ddfct(3,3,nrr,nth,nph)

   real (kind=8) :: dxiTmp(3,nrr,nth,nph)
   real (kind=8) :: ddxijTmp(3,3,nrr,nth,nph)

   real (kind=8) :: ank_fct(1:nmax,1:nrr) 

   real (kind=8) :: amp, sig, x0, y0, z0
   real (kind=8) :: tmax, t2max 

   integer       :: i,j,k, l,m,n
   integer       :: mx(3) 

   ! Set test function (off-centered gaussian)
   x0 = 2d0; y0 = 3d0; z0 = 1d0; amp = 2d0; sig = 25d0
   !$OMP PARALLEL WORKSHARE
   forall ( i=1:nrr, j=1:nth, k=1:nph ) test(i,j,k) = amp*exp( - ( (rr(i)*nx(j,k)-x0)**2 + (rr(i)*ny(j,k)-y0)**2 + (rr(i)*nz(j,k)-z0)**2  )/sig   ) 
   !$OMP END PARALLEL WORKSHARE

   ! Transform test function forward/backward
   call fct_to_coeff( test, ank_fct )
   call coeff_to_fct( ank_fct, test2)

   ! Compute relative num. error (max-norm)
   mx    = maxloc( abs( test - test2 )  ) 
   tmax  = test(mx(1),mx(2),mx(3)) 
   t2max = test2(mx(1),mx(2),mx(3)) 

   num_acc =  abs( tmax - t2max ) / tmax 

   ! Check 1. derivatives of test function
   call coeff_to_dfct( ank_fct, dfct )

   !$OMP PARALLEL WORKSHARE
   forall ( i=1:nrr, j=1:nth, k=1:nph ) dxiTmp(1,i,j,k) = - 2d0*(rr(i)*nx(j,k)-x0)/sig * test(i,j,k)
   forall ( i=1:nrr, j=1:nth, k=1:nph ) dxiTmp(2,i,j,k) = - 2d0*(rr(i)*ny(j,k)-y0)/sig * test(i,j,k)
   forall ( i=1:nrr, j=1:nth, k=1:nph ) dxiTmp(3,i,j,k) = - 2d0*(rr(i)*nz(j,k)-z0)/sig * test(i,j,k)
   !$OMP END PARALLEL WORKSHARE

   if ( present(dlevel) ) then
      print *, "df/dx: ", maxval( abs( dfct(1,:,:,:) - dxiTmp(1,:,:,:) ) )
      print *, "df/dy: ", maxval( abs( dfct(2,:,:,:) - dxiTmp(2,:,:,:) ) )
      print *, "df/dz: ", maxval( abs( dfct(3,:,:,:) - dxiTmp(3,:,:,:) ) )
   end if

   ! Check 2. derivatives of test function
   call coeff_to_ddfct( ank_fct, ddfct )
  
   !$OMP PARALLEL WORKSHARE
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddxijTmp(1,1,i,j,k) = test(i,j,k)*( ( 2d0*(rr(i)*nx(j,k)-x0)/sig )**2 - 2d0/sig ) 
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddxijTmp(2,2,i,j,k) = test(i,j,k)*( ( 2d0*(rr(i)*ny(j,k)-y0)/sig )**2 - 2d0/sig ) 
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddxijTmp(3,3,i,j,k) = test(i,j,k)*( ( 2d0*(rr(i)*nz(j,k)-z0)/sig )**2 - 2d0/sig ) 
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddxijTmp(1,2,i,j,k) = 4d0*(rr(i)*nx(j,k)-x0)*(rr(i)*ny(j,k)-y0)/sig**2 * test(i,j,k)                                                            
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddxijTmp(1,3,i,j,k) = 4d0*(rr(i)*nx(j,k)-x0)*(rr(i)*nz(j,k)-z0)/sig**2 * test(i,j,k)                                                            
   forall ( i=1:nrr, j=1:nth, k=1:nph ) ddxijTmp(2,3,i,j,k) = 4d0*(rr(i)*ny(j,k)-y0)*(rr(i)*nz(j,k)-z0)/sig**2 * test(i,j,k)                                                            
   ddxijTmp(2,1,:,:,:) = ddxijTmp(1,2,:,:,:); ddxijTmp(3,1,:,:,:) = ddxijTmp(1,3,:,:,:); ddxijTmp(3,2,:,:,:) = ddxijTmp(2,3,:,:,:) 
   !$OMP END PARALLEL WORKSHARE

   if ( present(dlevel) ) then
      print *,"d^2f/dx^2: ",  maxval( abs( ddfct(1,1,:,:,:) - ddxijTmp(1,1,:,:,:) ) )
      print *,"d^2f/dy^2: ",  maxval( abs( ddfct(2,2,:,:,:) - ddxijTmp(2,2,:,:,:) ) )
      print *,"d^2f/dz^2: ",  maxval( abs( ddfct(3,3,:,:,:) - ddxijTmp(3,3,:,:,:) ) )
   
      print *,"d^2f/dx/dy: ", maxval( abs( ddfct(1,2,:,:,:) - ddxijTmp(1,2,:,:,:) ) )
      print *,"d^2f/dx/dz: ", maxval( abs( ddfct(1,3,:,:,:) - ddxijTmp(1,3,:,:,:) ) )
      print *,"d^2f/dy/dz: ", maxval( abs( ddfct(2,3,:,:,:) - ddxijTmp(2,3,:,:,:) ) )
   end if

   !$OMP PARALLEL WORKSHARE
   check_sum = maxval( abs( dfct - dxiTmp ) ) + maxval( abs( ddfct - ddxijTmp ) )
   !$OMP END PARALLEL WORKSHARE

end subroutine test_spectral !}}} 

end module spectral_wkh

program spectral_wkh_prog !{{{ 

 use spectral_wkh

 implicit none

 call main

end program spectral_wkh_prog !}}}


